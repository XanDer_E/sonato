<?php
    define( 'SALT', 'masnymasnylettersasndlongstring777' );
    
    define( 'DB_HOST', 'localhost' );
    define( 'DB_USER', 'root' );
    define( 'DB_PASSWORD', '' );
    define( 'DB_NAME', 'sonato' );

	define( "CLASS_DIR", $_SERVER['DOCUMENT_ROOT'].'/classes' );
    define( "MODELS_DIR", CLASS_DIR.'/models' );
    define( "CONTROLLERS_DIR", CLASS_DIR.'/controllers' );
    define( "VIEWS_DIR", CLASS_DIR.'/views' );

    define( "PAGES_FOLDER", $_SERVER['DOCUMENT_ROOT'].'/pages'  );
    define( "JS_FOLDER", '/js' );
    define( "CSS_FOLDER", '/css' );
    define( "TPL_FOLDER", PAGES_FOLDER.'/templates' );
    define( "J_QUERY", 'jquery-3.0.0.min.js' );

	define( 'ACCESS_DENIED', 0);
	define( "ACCESS_GUEST", 1);
	define( "ACCESS_PATIENT_ADD", 2);
	define( "ACCESS_PATIENT_EDIT", 4);
	define( "ACCESS_SURVEY_ADD", 8 );
	define( "ACCESS_SURVEY_EDIT", 16);
	define( "ACCESS_USER_ADD", 32);
	define( "ACCESS_USR_DELETE", 64);
	define( "ACCESS_USR_LIST_VIEW", 128);
    define( "ACCESS_REGISTER" , ( ACCESS_USER_LIST_VIEW | ACCESSPATIENT_ADD | ACCESS_PATIENT_EDIT ) );
	define( "ACCESS_MEDIK" , ( ACCESS_REGISTER | ACCESS_SURVEY_ADD | ACCESS_SURVEY_EDIT ) );
	define( "ACCESS_ADMIN", ( ACCESS_USER_ADD | ACCESS_USR_DELETE | ACCESS_MEDIK | ACCESS_REGISTER ) );

	
// TODO: Сделать автоматическую обработку переменных-констант, чтобы можно было записывать только один раз

	/*
    _ФАМ_       _логин_ _пароль_    _хэш_
    
    Регистров   reggy   registrov   $2y$10$masnymasnylettersasndeK3QSJXww3IgTmbN.X9aPuWMjyDyPNAy
    Админ       admin   admin       $2y$10$masnymasnylettersasnde2jrka2aZh2yLJkfe1Rci6HkKZUfO8MK
    Медик       medik   medik       $2y$10$masnymasnylettersasnde2brvBw0VIS2mmsPvDTTfnOmp4HW3yZ2
    */