<!DOCTYPE html>
<html lang="{T:lang}">
<head>
    
        <title>{T:title}</title>
    <meta charset="{T:charset}" />
   <script src="https://use.fontawesome.com/e5bbf3867b.js"></script>
    {T:js}
    {T:css}
    
</head>

<body>

<!--   header -->
    
     <header>
{T:user_name} {T:exit_button}
          <h1 class="main-header">Sonato {T:pwd}</h1>
     </header>
  