-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 16 2016 г., 17:17
-- Версия сервера: 5.7.11
-- Версия PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `sonato`
--

-- --------------------------------------------------------

--
-- Структура таблицы `parametrs`
--

CREATE TABLE IF NOT EXISTS `parametrs` (
  `id` int(10) NOT NULL COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Наименование',
  `unit` varchar(255) NOT NULL COMMENT 'Тип/Единицы измерения'
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='Перечень параметров обследований';

--
-- Дамп данных таблицы `parametrs`
--

INSERT INTO `parametrs` (`id`, `name`, `unit`) VALUES
(1, 'Эритроциты (RBC)', '10^12 клеток/л'),
(2, 'Гемоглобин (HGB)	', 'г/л'),
(3, 'Гематокрит (HCT)', '%'),
(4, 'Средний объем эритроцита (MCV)', 'фл'),
(5, 'Среднее содержание гемоглобина в эритроците (МСН)', 'пг'),
(6, 'Средняя концентрация гемоглобина в эритроците (МСНС)', 'г/дл'),
(7, 'Цветовой показатель (ЦП)', '-'),
(8, 'Ретикулоциты (RTC)', '%'),
(9, 'Тромбоциты (PLT)', '109 клеток/л'),
(10, 'Лейкоциты (WBC)', '109 клеток/л'),
(11, 'Сегментоядерные нейтрофилы (NEU)', '%'),
(12, 'Палочкоядерные нейтрофилы (Bands)', '%'),
(13, 'Миелоциты (Mie)', '%'),
(14, 'Метамиелоциты (юные)', '%'),
(15, 'Лимфоциты (LYM)', '%'),
(16, 'Моноциты (MO)', '%'),
(17, 'Эозинофилы (EOS)', '%'),
(18, 'Базофилы (BA)', '%'),
(19, 'Атипичные мононуклеары (вироциты)', '%'),
(20, 'Плазматические клетки (плазмоциты)', '%'),
(21, 'СОЭ (ESR)', '%'),
(22, 'Диагноз', '-'),
(23, 'Жалобы пациента', '-'),
(24, 'характер кровотока', 'текст'),
(25, 'Скорость кровотока в систолу', 'текст'),
(26, 'Скорость кровотока в диастолу', 'текст'),
(27, 'Соотношение между Vmax и Vmin', 'текст'),
(28, 'Форма спектральной волны', 'текст'),
(29, 'Процент стеноза артерии', 'текст'),
(30, 'Индекс резистентности', ''),
(31, 'Пульсаторный индекс', 'текст');

-- --------------------------------------------------------

--
-- Структура таблицы `patients`
--

CREATE TABLE IF NOT EXISTS `patients` (
  `id` int(10) NOT NULL COMMENT 'Идентификатор',
  `first_name` varchar(60) NOT NULL COMMENT 'Имя',
  `last_name` varchar(60) NOT NULL COMMENT 'Фамилия',
  `patronymic` varchar(60) NOT NULL DEFAULT 'Не указано' COMMENT 'Отчество',
  `sex` varchar(1) NOT NULL COMMENT 'Пол',
  `mother_fio` varchar(255) NOT NULL COMMENT 'ФИО матери',
  `m_wk_place_` varchar(255) NOT NULL COMMENT 'Место работы матери',
  `f_fio` varchar(255) NOT NULL COMMENT 'ФИО папы',
  `m_wk_place` varchar(255) NOT NULL COMMENT 'Место работы папы',
  `reg_address` varchar(255) NOT NULL DEFAULT 'Не указано' COMMENT 'Адрес прописки',
  `fact_address` varchar(255) NOT NULL DEFAULT 'Не указано' COMMENT 'Адрес фактического проживания',
  `phone` varchar(11) NOT NULL COMMENT 'телефон',
  `birth_cert_number` varchar(16) NOT NULL COMMENT 'Номер свидетельства о рождении',
  `courier` varchar(60) NOT NULL COMMENT 'Доставил',
  `arrived` date NOT NULL COMMENT 'Прибыл',
  `lated` int(4) NOT NULL COMMENT 'Опоздал',
  `begin_surv_date` date NOT NULL COMMENT 'Дата заселения',
  `end_surv_date` date NOT NULL COMMENT 'Дата выписки',
  `treatment_extends` int(11) NOT NULL,
  `discharged_days` int(11) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `days_spend` int(3) NOT NULL,
  `sick_list_since` date NOT NULL,
  `sick_list_to` date NOT NULL,
  `taker` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Пациенты';

--
-- Дамп данных таблицы `patients`
--

INSERT INTO `patients` (`id`, `first_name`, `last_name`, `patronymic`, `sex`, `mother_fio`, `m_wk_place_`, `f_fio`, `m_wk_place`, `reg_address`, `fact_address`, `phone`, `birth_cert_number`, `courier`, `arrived`, `lated`, `begin_surv_date`, `end_surv_date`, `treatment_extends`, `discharged_days`, `reason`, `days_spend`, `sick_list_since`, `sick_list_to`, `taker`) VALUES
(1, 'Иванов', 'Иван', 'Иванович', 'м', 'Иванова Мария Ивановна', '"ООО мастерские"', 'Иванов Иван Николаевич', 'Газпром', 'г. Архангельск, ул. Красивых молдавских партизан, д. 777, кв. 11', 'г. Архангельск, ул. Красивых молдавских партизан, д. 777, кв. 11', '151617', '2382574834787478', 'Мама', '2016-06-01', 0, '2016-06-02', '2016-07-14', 0, 0, '-', 58, '0000-00-00', '0000-00-00', 'Папа'),
(2, 'Пауль', 'Ландерс', 'Не указано', '', '', '', '', '', 'г. Берлин', 'Нью-Йорк', '', '', '', '0000-00-00', 0, '2016-06-01', '2016-11-04', 0, 0, '', 0, '0000-00-00', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Структура таблицы `surveys_catalog`
--

CREATE TABLE IF NOT EXISTS `surveys_catalog` (
  `id` int(10) NOT NULL COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Наименование',
  `id_param_fk` int(10) NOT NULL COMMENT 'Идент. параметра'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Справочник обследований';

--
-- Дамп данных таблицы `surveys_catalog`
--

INSERT INTO `surveys_catalog` (`id`, `name`, `id_param_fk`) VALUES
(1, 'Анализ крови первого типа', 3),
(2, 'Анализ крови второго типа (выборочный)', 2),
(3, 'УЗИ стопы', 5),
(4, 'Дуплекс', 7);

-- --------------------------------------------------------

--
-- Структура таблицы `surveys_journal`
--

CREATE TABLE IF NOT EXISTS `surveys_journal` (
  `id` int(10) NOT NULL COMMENT 'id',
  `surv_code_fk` int(10) NOT NULL COMMENT 'Код обследования',
  `id_people_fk` int(10) NOT NULL COMMENT 'Код пациента',
  `id_specialist_fk` int(10) NOT NULL COMMENT 'Код специалиста'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `surveys_journal`
--

INSERT INTO `surveys_journal` (`id`, `surv_code_fk`, `id_people_fk`, `id_specialist_fk`) VALUES
(1, 1, 1, 1),
(2, 4, 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `survey_results`
--

CREATE TABLE IF NOT EXISTS `survey_results` (
  `id` int(10) NOT NULL COMMENT 'id',
  `id_survey_fk` int(10) NOT NULL COMMENT 'Код обследования (внешний ключ)',
  `id_param_fk` int(10) NOT NULL COMMENT 'Код параметра (внешний ключ)',
  `result_value` varchar(255) NOT NULL COMMENT 'Результаты обследования'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Журнал проведенных обследований';

--
-- Дамп данных таблицы `survey_results`
--

INSERT INTO `survey_results` (`id`, `id_survey_fk`, `id_param_fk`, `result_value`) VALUES
(3, 2, 13, 'Результат номер 1'),
(4, 1, 9, 'Результат номер 2'),
(5, 2, 5, 'Результат номер 3');

-- --------------------------------------------------------

--
-- Структура таблицы `treatments`
--

CREATE TABLE IF NOT EXISTS `treatments` (
  `id` int(10) NOT NULL COMMENT 'id',
  `name` varchar(255) NOT NULL COMMENT 'Наименование лечебной процедуры',
  `id_surv_fk` int(10) NOT NULL COMMENT 'ID обследования по которому назначено лечение'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Лечебные процедуры';

--
-- Дамп данных таблицы `treatments`
--

INSERT INTO `treatments` (`id`, `name`, `id_surv_fk`) VALUES
(1, 'Рекомендуется много спать, не нервничать и гулять в грязевых ваннах', 2),
(2, 'Электрошоковая терапия', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL COMMENT 'id',
  `login` varchar(255) NOT NULL COMMENT 'Логин',
  `password` varchar(255) NOT NULL COMMENT 'Пароль',
  `right_mask` int(10) NOT NULL COMMENT 'Битовая маска прав',
  `first_name` varchar(60) NOT NULL DEFAULT 'Не определено' COMMENT 'Имя',
  `second_name` varchar(60) NOT NULL DEFAULT 'Не определено' COMMENT 'Фамилия',
  `patronymic` varchar(60) NOT NULL DEFAULT 'Не определено' COMMENT 'Отчество',
  `speciality` varchar(255) NOT NULL COMMENT 'Специальность/должность',
  `cabinet` varchar(255) NOT NULL COMMENT 'Кабинет'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Таблица сотрудников';

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `right_mask`, `first_name`, `second_name`, `patronymic`, `speciality`, `cabinet`) VALUES
(1, 'admin', '$2y$10$masnymasnylettersasnde9o5nuaTZHAbgVHuCg33dX/4fADeGjKO', 124, 'Не определено', 'Не определено', 'Не определено', 'Администратор', '404'),
(2, 'medik', '$2y$10$masnymasnylettersasndey8YiJtGyi6Fwa7zWbWsgzN0ozs99uxe', 28, 'Медик', 'Иван', 'Федорович', 'Хирург', '209'),
(3, 'reggy', '$2y$10$masnymasnylettersasndeLsWomxeKthRUxmE02JiHlRIWLTyGFfW', 4, 'Регистров', 'Пасс', 'Аркадьевич', 'регистратор', '002'),
(4, 'pritchard', '$2y$10$masnymasnylettersasnde9o5nuaTZHAbgVHuCg33dX/4fADeGjKO', 96, 'Фрэнсис', 'Притчард', 'Уэнделл', 'Консультант', '201'),
(5, 'qwert', '$2y$10$masnymasnylettersasnde9o5nuaTZHAbgVHuCg33dX/4fADeGjKO', 123, 'Alex', 'Joseph', 'Lex', 'none', '310-a');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `parametrs`
--
ALTER TABLE `parametrs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `surveys_catalog`
--
ALTER TABLE `surveys_catalog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_param` (`id_param_fk`);

--
-- Индексы таблицы `surveys_journal`
--
ALTER TABLE `surveys_journal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `surv_code_fk` (`surv_code_fk`),
  ADD KEY `id_people_fk` (`id_people_fk`),
  ADD KEY `id_specialist_fk` (`id_specialist_fk`);

--
-- Индексы таблицы `survey_results`
--
ALTER TABLE `survey_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_survey_fk` (`id_survey_fk`),
  ADD KEY `id_param_fk` (`id_param_fk`);

--
-- Индексы таблицы `treatments`
--
ALTER TABLE `treatments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_surv_fk` (`id_surv_fk`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `parametrs`
--
ALTER TABLE `parametrs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT для таблицы `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор',AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `surveys_catalog`
--
ALTER TABLE `surveys_catalog`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `surveys_journal`
--
ALTER TABLE `surveys_journal`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `survey_results`
--
ALTER TABLE `survey_results`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `treatments`
--
ALTER TABLE `treatments`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=6;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `surveys_catalog`
--
ALTER TABLE `surveys_catalog`
  ADD CONSTRAINT `surveys_catalog_ibfk_1` FOREIGN KEY (`id_param_fk`) REFERENCES `parametrs` (`id`);

--
-- Ограничения внешнего ключа таблицы `surveys_journal`
--
ALTER TABLE `surveys_journal`
  ADD CONSTRAINT `surveys_journal_ibfk_1` FOREIGN KEY (`surv_code_fk`) REFERENCES `surveys_catalog` (`id`),
  ADD CONSTRAINT `surveys_journal_ibfk_2` FOREIGN KEY (`id_people_fk`) REFERENCES `patients` (`id`),
  ADD CONSTRAINT `surveys_journal_ibfk_3` FOREIGN KEY (`id_specialist_fk`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `survey_results`
--
ALTER TABLE `survey_results`
  ADD CONSTRAINT `survey_results_ibfk_1` FOREIGN KEY (`id_survey_fk`) REFERENCES `surveys_journal` (`id`),
  ADD CONSTRAINT `survey_results_ibfk_2` FOREIGN KEY (`id_param_fk`) REFERENCES `parametrs` (`id`);

--
-- Ограничения внешнего ключа таблицы `treatments`
--
ALTER TABLE `treatments`
  ADD CONSTRAINT `treatments_ibfk_2` FOREIGN KEY (`id_surv_fk`) REFERENCES `surveys_journal` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
