<?php
defined('ENGINE_ADM') or die('Access denied');

class Admin_Controller extends Root_Controller
{
    function init(){
        $this->db -> setConnectionParams( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME );
        $this->db -> connectDB( DB_NAME );
        $result = $this->db->query("SELECT * FROM `users`");
        
        $tbody = array();
        while ( $row = mysql_fetch_row( $result ) ) {
            $tbody[] = $row;
        }
        
        $thead = array('id',
                       'Логин',
                       'Хэш пароля',
                       'Права',
                       'Имя',
                       'Фамилия', 
                       'Отчество', 
                       'Специальность', 
                       'Кабинет');

        // Отдаем в предсталвение таблицу с данными из базы
        $users = $this->view->table( $thead, $tbody, 'list user-list' );
        $this->view->addData('users', $users);
        $this->view->addData('user_name', '<span class="u-name">'.$_SESSION['login'].'</span>');
        echo $this->view->displayAdminDefaultPage();
        
        
        
        
       // echo "КОНТРОЛЛЕР ".__CLASS__."<br>";
    }

}