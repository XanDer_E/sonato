<?php
defined('ENGINE_ADM') or die('Access_denied');


require_once(MODELS_DIR.'/error.php');
require_once(MODELS_DIR.'/user.php');
require_once(MODELS_DIR.'/database.php');
require_once(VIEWS_DIR.'/view.php');

class Root_Controller
{
    protected $user_errors;
    protected $sys_errors;
    protected $headers;
    protected $user;
    protected $db;
    protected $view;
    
    function __construct() {
        $this->usr_errors = new Errors;
        $this->sys_errors = new Errors;
        $this->headers = array();
        $this->user = new User;
        $this->view = new View;
        $this->db = new Database;
        
        $this->init();
    }
    
    // ЗАПУСК КОНТРОЛЛЕРА
    protected function init(){
        session_start();
        ob_start();
        echo "Работает класс".__CLASS__;
    }
    
        // Получение запроса пользователя
    private function getUserAction() {
        if ( isset( $_REQUEST['action'] ) ) {
            $this->user->action = $_REQUEST['action'];
            return $this->user->action;
        } else {
            return false;
        }
    }
    
    protected function sendHeaders(){
        foreach( $this->headers as $key => $value ) {
            header( $value );
        }
    }
    
    protected function addHeader( $heade ){
        $this->headers[] = $heade;
    }
    
    protected function redirect($addr) {
        $this->addHeader( 'Location: index.php?page='.$addr );
    }
    
    protected function setDefaultPage() {
        switch($_SESSION['role']) {
            case ACCESS_ADMIN; $this->default_page = 'admin'; break;
            case ACCESS_MEDIK; $this->default_page = 'patient-list'; break;
            case ACCESS_REGISTER; $this->default_page = 'register'; break;
            case ACCESS_DENIED; $this->default_page = 'auth'; break;
        }
//       var_dump($_SESSION['role']);
        $_SESSION[ 'default_page' ] = $this->default_page;
        return $this->default_page;
    }
    
    protected function logout(){
        session_destroy();
        $this->redirect('auth');
    }
}

?>