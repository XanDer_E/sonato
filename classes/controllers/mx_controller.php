<?php	
defined('ENGINE_ADM') or die('Access denied'); // Проверка точки входа

require_once( $_SERVER['DOCUMENT_ROOT'].'/definitions.php' ); // Подключаем "словарь"
require_once( CONTROLLERS_DIR.'/root-controller.php' ); // Подключаем главный контроллер

class MX_Controller extends Root_Controller
{
    protected $user_errors;
    protected $sys_errors;
    protected $user;
    
    // Функция выбора контроллера
    private function getController( $page ) {

        switch ($page) {
            case 'auth': include CONTROLLERS_DIR.'/auth_controller.php';
                         $controller = new Auth_Controller;
                         break;
            case 'register': include CONTROLLERS_DIR.'/register_controller.php';
                         $controller = new Register_Controller;
                         break;
            case 'patient-list': include CONTROLLERS_DIR.'/patient-list-controller.php';
                         $controller = new Patient_List_Controller;
                         break;
            case 'admin': include CONTROLLERS_DIR.'/admin-controller.php';
                         $controller = new Admin_Controller;
                         break;
            case 'add-survey': include CONTROLLERS_DIR.'/add-survey.php';
                         $controller = new Survey_Add_Controller;
                         break;
                // Если контроллер не определен
            default: $controller = NULL; break; 
        }

        return $controller;
    }
    
    // Запуск выбранного контроллера
    public function init() {
        session_start();    // Инициализация сессии 
        ob_start();         // Инициализация буфера вывода
        // Если авторизован
        if ( isset( $_SESSION['authorized'] ) ) { // ЕСЛИ АВТОРИЗОВАН
                    
            if ( isset( $_REQUEST['page'] ) and ( $_REQUEST['page'] != 'auth' ) ) {
                $controller = $this->getController($_REQUEST['page']);

            } else { 
                $controller = $this->getController($_SESSION['default_page']); // Если не указан путь, или переход на страницу авторизации (будучи авторизованным), то перенаправление на страницу по умолчанию.
            }
        } else { // Если не авторизован
            
            $controller = $this->getController('auth');
        }

        if ( $controller === NULL ) {
            $view = new View;
            $view -> displayPage('404');
        }
        
        ob_end_flush();
    }
    
}