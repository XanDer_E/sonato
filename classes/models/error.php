<?php
defined('ENGINE_ADM') or die('Access denied');
class Errors
{
    private $error_list;
    
    function __construct() {
        $error_list = array();
    }
    
    public function add( $code, $msg ){
        if($code != ''){
            $this->errors_list[][$code]['msg'] = $msg;
            return $code;
        } else {
            return false;
        }
    }
    
    public function _list() {
        if( empty( $this->error_list ) ) {
            return false;
        } else {
            return $this->error_list;
        }
    }
    
} // class System_Errors ends