<?php
defined('ENGINE_ADM') or die('Access denied');

class User
{
	// Свойства:
	private $name;
	private $login;
	private $password;
	private $role;
    
    
    // Конструктор. Инициализация свойств
    function __construct(){
        $name = false;
        $login = false;
        $password = false;
        $role = false;
    }
    
	// Методы:
	function setName($name) {
		$this->name = $name;
		return $this->name;
	}
	
	function getName() {
		return $this->name;
	}
	
	/* Получить роль пользователя */
	function getRole( $login ) {
        $DB = new Database();
        $DB -> setConnectionParams( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME );
        $DB -> connectDB( DB_NAME );
        $res = $DB->query("SELECT `right_mask` FROM `users` WHERE `login` LIKE '".$login."'");
        $result = mysql_fetch_row( $res );
        $this->role = $result[0];
		return $this->role;
	}
	
    /* Установка пароля юзера */
	function setPassword($uid, $pass) {
        $DB = new Database();
        $DB -> setConnectionParams( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME );
        $DB -> connectDB( DB_NAME );
        $this->password = $DB -> query( 'UPDATE `sonato`.`users` SET `password` = \''.$pass.'\' WHERE `users`.`id` ='.$uid.';' );
		return $this->password;
	} //UPDATE `sonato`.`users` SET `password` = '$2y$10$masnymasnylettersasnde9o5nuaTZHAbgVHuCg33dX/4fADeGjKO' WHERE `users`.`id` = 1;
	
	function getPassword() {
		return $this->password;
	}
	
	function getLogin() {
		return $this->login;
	}
	
	function isLogined(){
		$result = ( isset( $_SESSION['authorized'] ) ) ? true : false;
		return $result;
	}
	
    public function isAdmin() {
        return ( $this->getRole() == ACCESS_ADMIN );
    }

    public function isRegister() {
        return ($this->getRole() == ACCESS_REGISTER );
    }	

    public function isMedik() {
        return ( $this->getRole() == ACCESS_MEDIK );
    }
    
    private function rightsAdd($uid, $rights) {
        switch ($rights) {
            case "admin": 
                $new_rights = ACCESS_ADMIN | $rights; break;
            case "receptionist": 
                $new_rights = ACCESS_RECEPTIONIST | $rights; break;
            case "medik": 
                $new_rights = ACCESS_MEDIK | $rights; break;
            case "denied": 
                $new_rights = ACCESS_DENIED | $rights; break;
            case "survey_add": 
                $new_rights = ACCESS_SURVEY_ADD | $rights; break;
            case "survey_edit": 
                $new_rights = ACCESS_SURVEY_EDIT | $rights; break;
            case "usr_add": 
                $new_rights = ACCESS_USER_ADD | $rights; break;
            case "usr_delete": 
                $new_rights = ACCESS_USR_DELETE | $rights; break;
            
        $DB = new Database();
        $DB -> setConnectionParams( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME );
        $DB -> connectDB( DB_NAME );
        $this->role = $DB -> query( 'UPDATE `sonato`.`users` SET `right_mask` = \''.$new_rights.'\' WHERE `users`.`id` ='.$uid.';' );
		
            return $this->role;
        }
    }
    
    /* Провека данных юзера */
    public function checkUserData( $login, $password ) {
        $DB = new Database();
        $DB -> setConnectionParams( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME );
        $DB -> connectDB( DB_NAME );
        $res = $DB->query( "SELECT `login`,`password` 
                       FROM `users` 
                       WHERE `login` LIKE '".$login."'" );
        if (!$res) {
            return false;
        }
        
        $fetched = mysql_fetch_row( $res );
        $result = ( password_verify( $password, $fetched[1] ) ) ? true : false;
        unset( $DB );
        return $result;
    }
    
    /* Вход в систему */
    public function login( $login, $password ){

        if ( $this->checkUserData( $login, $password ) ) {
            $_SESSION['authorized'] = true;
            $_SESSION['login'] = $this -> login;
            $_SESSION['password'] = $this -> password;
            $_SESSION['role'] = $this -> getRole( $this->login );
           // var_dump($_SESSION['role']);
            return true;
        } else {
            return false;
        }
        
    }
    
} // class User ends