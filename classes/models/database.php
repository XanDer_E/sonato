<?php
defined('ENGINE_ADM') or die('Access denied');

class Database
{
	// Свойства:
	private $host;			// Имя хоста
	private $db_user;		// Пользователь БД
	private $password;		// Пароль юзера
	private $db;			// Название БД
	private $link;			// Соединение
	
	// Конструктор:
	function __construct() {
		$this->host = '';
		$this->db_user = '';
		$this->password = '';
		$this->db = '';
		$this->link = '';
	}
	
	// Методы:
	
    
    /* Запрос */
	public function query($query) {
		$this->sql_result = mysql_query($query);
        return $this->sql_result;
	}
	
    /* Установка параметров соединения */
    public function setConnectionParams( $host, $db_user, $password, $db ) {
        $this->host = $host;      
		$this->db_user = $db_user;      
		$this->password = $password;    
		$this->db = $db;         
    }
    
	/* Установка соединения */
	public function connect() {
        $this->link = @mysql_connect($this->host, $this->db_user, $this->password);
		@mysql_query("set character_set_client='utf8'");
		@mysql_query("set character_set_results='utf8'");
		@mysql_query("set collation_connection='utf8_general_ci'");
		@mysql_query("SET NAMES utf8");
        return $this->link;
	}
	
    /* Выбор БД */
    public function select_db( $db ) {
        $this->db = $db;
		return @mysql_select_db($db, $this->link);
    }
    
    /* Подключение и выбор БД (ОБЕРТКА connect() и select_db( $db ) ) */
    public function connectDB( $db ) {
        return ( $this->connect() and $this->select_db( $db ) );
    }
    
	/* Запрос на добавление данных в таблицу
		(обертка метода query, и запроса insert) */
		
	public function insert( $table, $values = array() ) {
		/* HowTo:
			insert( `ТаблицаПараметры`, [ 'id'   => 'NULL',
										  'name' => "'Жалобы'",
								          'unit' => '-' ] ); */
		
		$fields = '';
		$vals = '';
		
		foreach ( $values as $k => $v ) {
			
			$fields = ( $fields == '' ) ? '`'.$k.'`' : ', `'.$k.'`';
			$vals  = ( $vals  == '' ) ? ''.$v : ', '.$v;
			
		}
		$sql = 'INSERT INTO `'.$this->db.'`.`'.$table.'` ('.$fields.') VALUES ('.$vals.')';
		$this->query($sql);
	}
    
    public function update(){
        /* HowTo:
			insert( `ТаблицаПараметры`, [ 'id'   => 'NULL',
										  'name' => "'Жалобы'",
								          'unit' => '-' ] ); */
        //UPDATE `sonato`.`users` SET `password` = '$2y$10$masnymasnylettersasnde9o5nuaTZHAbgVHuCg33dX/4fADeGjKO' WHERE `users`.`id` = 1;
        /*update( `ТаблицаЮзеры`, `field` => 'ddafaadf', '`users`.`id` = 1' )*/
    }

    /* Получить */
	public function checkUserData( $login, $password ) {
        $res = $this->query( "SELECT `login`,`password` 
                       FROM `users` 
                       WHERE `login` LIKE '".$login."'" );
        if(!$res){
            return false;
        }
        $fetched = mysql_fetch_row( $res );
        $result = ( password_verify( $password, $fetched[1] ) ) ? true : false;
    //    echo password_hash('pass12345', PASSWORD_BCRYPT, ['salt' => SALT]);
      //  var_dump($result);
//        if (password_verify( $password, $fetched[1]) ){
//    echo 'Password is valid!: '.$fetched[1];
//} else {
////    echo 'Invalid password.';
//    echo 'Password is INvalid!: '.$fetched[1];
//} 
        /*echo '<pre>';
        var_dump($result);
        echo '</pre>';*/
        return $result;
    }
    
    public function getUserRole($login){
        $res = $this->query("SELECT `right_mask` FROM `users` WHERE `login` LIKE '".$login."'");
        $result = mysql_fetch_row( $res );
        return $result[0];
    }
    
    public function addUser(){
        
    }
    
    
	/*...*/
}