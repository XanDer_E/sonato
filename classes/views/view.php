<?php
defined('ENGINE_ADM') or die('Access denied');

class View
{
    private $data = array();
    
    private $charset = '';
    private $title = '';
    

    function addData( $key, $value ) {
        if( $this->data[$key] = $value ) {
            return true;
        } else {
            return false;
        }
    }
    
    function getData( $key ) {
        return $this->data[$key];
    }
    
    function displayErrorsArray($errors = array() ) {
        echo '<div>';
        foreach ($errors as $k => $v) {
            echo "<p>{$v}</p>";
        }
        
        echo '</div>';
    }

    
function setHead( $charset, $title, $styles = array(), $js = array() ) {
    
    $this->head = '<head>';
    $css = '';
    foreach ($styles as $k => $v) {
        $styles .= $this->linkStylesheets($v);
    }
    
    foreach ($js as $k => $v) {
        $this->head .= $this->linkJsScript($v);
    }
    
    $this->head .= '</head>';
}
    
function getHead(){
    $tpl = new Templator();
    $tpl->loadTemplate($template);
    $tpl->loadData([ 'charset' => 'utf-8',
                     'lang' => 'ru',
                     'title' => 'Sonato',
                     'css' => $action,
                     'js' => $template ]);
    $form = $tpl->processData();

    return $form;

    
    return $this->head;
}
    
function setTitle($title){
    $this->title = '<title>'.$title.'</title>';
}   

function linkJsScript($filename){
    return '<script src="'.JS_FOLDER.'/'.$filename.'"></script>';
}

function linkStylesheets($filename){
    return '<link rel="stylesheet" href="'.CSS_FOLDER.'/'.$filename.'">';
}    

function setHeader() {
    $this->pageHeader = '<h1>'.$this->getTitle().'</h1>';
}

function getHeader() {
    return $this->pageHeader;
}

function setFooter($content) {
    $this->pageHeader = '<footer>'.$content.'</footer>';
}

function getFooter() {
    return $this->pageHeader;
}
    
    
    
    //**************************************************************
    function __construct() {
        require_once VIEWS_DIR.'/templator.php';
        ob_start();
    }

	function displayDefault() {
	
	}
	
	function displayError($error) {
		echo "<p><b>Ошибка:</b> {$error}";
	}
	
	function displayResults($results) {
	
		echo "<p><b>Результаты:</b>";
		echo "<p>Ваше имя <b>".$results[0].
			"</b> означает <i>".$results[1]."</i>";
		echo "<p><a href='".$_SERVER['REQUEST_URI'].
					"'>Узнать ещё об одном имени</a>";
	}	

    function getTemplate($tpl) {
        if( file_exists($path = TPL_FOLDER.'/'.$tpl.'.tpl') ) {
            return file_get_contents( $path );
        } else {
            return false;
        }
    }
    
    function getSubmitButton($form, $text){
       return '<div class="btn" onClick="javascript:document.'.$form.'.submit()">'.$text.'</div>';
    }
    
    function getButton($href, $text, $class){
       return '<div class="btn '.$class.'" onClick="document.location.href=\''.$href.'">'.$text.'</div>';
    }
    
    function displayPage($page) {
        switch($page) {
            case '404': $page = file_get_contents(PAGES_FOLDER."/404.html"); break;
            default: $page = 'НАШЛАСЬ';
        }
        echo $page;
    }
    
    function getForm($id_form, $name, $method, $action, $template) {
        $tpl = new Templator();
        $tpl->loadTemplate($template);
        $tpl->loadData([ 'name' => $name,
                         'id' => $id_form,
                         'method' => $method,
                         'action' => $action,
                         'template' => $template ]);
        $form = $tpl->processData();

        return $form;
    }
    
    function getAuthPage($access) {
        $tpl = new Templator;
        $tpl -> loadTemplate('authorization');
        $js = $this->linkJsScript( J_QUERY );
        $js .= $this->linkJsScript( 'usr.js' );
        $js .= $this->linkJsScript( 'jquery.shuffleLetters.js' );
        $css = $this->linkStylesheets( 'reset.css' );
        $css .= $this->linkStylesheets( 'main.css' );
        $css .= $this->linkStylesheets( 'auth.css' );
        $css .= $this->linkStylesheets( 'header.css' );
        $css .= $this->linkStylesheets( 'footer.css' );
        
        switch($access) {
            case 'await':   $acc = ''; 
                            $legend = 'Пожалуйста, авторизуйтесь'; 
                            break;
                
            case 'denied':  $acc = 'access-denied'; 
                            $legend = 'Доступ запрещен'; 
                            break;
        }

        $tpl -> loadData([  'header' => $this->getTemplate('header'),
                            'css' => $css, 'js' => $js,
                            'charset' => 'utf-8',
                            'lang' => 'ru',
                            'title' => 'Sonato',
                            'footer' => $this->getTemplate('footer'),
                            'id' => 'authorize',
                            'pwd' => '/ Вход в систему',
                            'name' => 'authorize',
                            'action' => 'index.php?action=auth',
                            'method' => 'post',
                            'auth_form' => $this->getTemplate('auth_form'),
                            'access' => $acc,
                            'exit_button' => '',
                            'submit_button' => $this->getSubmitButton("authorize","Вход"),
                            'user_name' => '',
                            'legend' => $legend
                         ]);
       return $tpl -> processData();
        
    }
    
    function tr( $arr = array() ) {
        $out = '<tr>';
        foreach ( $arr as $k => $value) {
            $out .= '<td>'.$value.'</td>';
        }
        $out .= '</tr>';
        return $out;
    }
    
    
    function displayPatientList( $data = array() ) {
        /*<>
        foreach( $data as $key => $value ) {
            echo $this->tr( $value );
        }*/
        
        $tpl = new Templator;
        $tpl -> loadTemplate('patient-list');
        $js = $this->linkJsScript( J_QUERY );
        $js .= $this->linkJsScript( 'usr.js' );
        $js .= $this->linkJsScript( 'jquery.shuffleLetters.js' );
        $css = $this->linkStylesheets( 'reset.css' );
        $css .= $this->linkStylesheets( 'main.css' );
        $css .= $this->linkStylesheets( 'patient-list.css' );
        $css .= $this->linkStylesheets( 'header.css' );
        $css .= $this->linkStylesheets( 'footer.css' );
        $tpl -> loadData([  'header' => $this->getTemplate('header'),
                            'css' => $css, 'js' => $js,
                            'charset' => 'utf-8',
                            'lang' => 'ru',
                            'title' => '{T:pwd}| АРМ Sonato',
                            'footer' => $this->getTemplate('footer'),
                            'search' => ' <div class="s-button"><i class="fa fa-search fa-2x" aria-hidden="true"></i></div>',
                            'pwd' => '/ Список отдыхающих',
//                            'patient_list_table' => $this->getTemplate('patient-list'),
                           // 'submit_button' => $this->getSubmitButton("authorize","Вход"),
                            'user_name' => '<div class="u-name"><i class="fa fa-user" aria-hidden="true"></i>'.$this->getData('user_name').'</div>',
                            'exit_button' => $this->getButton("index.php?action=logout",'<i class="fa fa-power-off" aria-hidden="true"></i> Выход', "exit-button"),
                            'submit_button' => $this->getButton("reg",'<i class="fa fa-pencil" aria-hidden="true"></i> Изменить', 'submit'),
                            'cancel_button' => $this->getButton("reg",'<i class="fa fa-repeat" aria-hidden="true"></i> Отменить', 'cancel')
                         ]);
       return $tpl -> processData();
        
    }
    
    function table ( $thead, $rows, $class='', $id=''  ) {
        $out = '<table';
        $out .= ( $id != '') ? ' id="'.$id.'"' : '';
        $out .= ( $class != '') ? ' class="'.$class.'"' : '';
        $out .= '>';
        $out .= '<thead>';
        
            $out .= $this->tr( $thead );
        
        
        $out .= '</thead>';
        $out .= '<tbody>';
        
        foreach( $rows as $k => $v ){
            $out .= $this->tr( $v );
        }
        $out .= '</tbody>';
        $out .= '</table>';
        return $out;
    }
    
   /* function link_table($thead, $rows, $class='', $id='') {
    $out = '<table';
        $out .= ( $id != '') ? ' id="'.$id.'"' : '';
        $out .= ( $class != '') ? ' class="'.$class.'"' : '';
        $out .= '>';
        $out .= '<thead>';
        
            $out .= $this->tr( $thead );
        
        
        $out .= '</thead>';
        $out .= '<tbody>';
        
        foreach( $rows as $k => $v ){
            $out .= $this->tr( $v );
        }
        $out .= '</tbody>';
        $out .= '</table>';
        return $out;
    }
    */
    function displayAdminDefaultPage(){
        $tpl = new Templator;
        $tpl -> loadTemplate('admin-default-page');
        $js = $this->linkJsScript( J_QUERY );
        $js .= $this->linkJsScript( 'usr.js' );
        $js .= $this->linkJsScript( 'jquery.shuffleLetters.js' );
        $css = $this->linkStylesheets( 'reset.css' );
        $css .= $this->linkStylesheets( 'main.css' );
        $css .= $this->linkStylesheets( 'admin.css' );
        $css .= $this->linkStylesheets( 'header.css' );
        $css .= $this->linkStylesheets( 'footer.css' );
        $tpl -> loadData([  'header' => $this->getTemplate('header'),
                            'css' => $css, 'js' => $js,
                            'charset' => 'utf-8',
                            'lang' => 'ru',
                            'title' => 'Sonato {T:pwd}',
                            'footer' => $this->getTemplate('footer'),
                            'pwd' => '/ Администрирование',
                            'users' => $this-> getData('users'),
'user_name' => '<div class="u-name"><i class="fa fa-user" aria-hidden="true"></i>'.$this->getData('user_name').'</div>',
                            'exit_button' => $this->getButton("index.php?action=logout",'<i class="fa fa-power-off" aria-hidden="true"></i> Выход', "exit-button"),
                            'submit_button1' => $this->getButton("reg",'<i class="fa fa-pencil" aria-hidden="true"></i> Изменить', 'submit'),
                            'submit_button2' => $this->getButton("reg",'<i class="fa fa-plus" aria-hidden="true"></i> Добавить', 'cancel')
                            
                         ]);
        
       return $tpl -> processData();
    }
    
    /*Страница добавления пациента*/
    function displayRegisterDefaultPage(){
        $tpl = new Templator;
        $tpl -> loadTemplate('add-patient');
        $js = $this->linkJsScript( J_QUERY );
        $js .= $this->linkJsScript( 'usr.js' );
        $js .= $this->linkJsScript( 'jquery.shuffleLetters.js' );
        $css = $this->linkStylesheets( 'reset.css' );
        $css .= $this->linkStylesheets( 'main.css' );
        $css .= $this->linkStylesheets( 'add-patient.css' );
        $css .= $this->linkStylesheets( 'header.css' );
        $css .= $this->linkStylesheets( 'footer.css' );

        $tpl -> loadData([  'header' => $this->getTemplate('header'),
                            'css' => $css, 'js' => $js,
                            'charset' => 'utf-8',
                            'lang' => 'ru',
                            'title' => 'Sonato {T:pwd}',
                            'footer' => $this->getTemplate('footer'),
                            'pwd' => '/ Новый посетитель',
                            'users' => $this-> getData('users'),
                            'id' => 'reg',
                            'name' => 'reg',
                            'action' => 'index.php?action=register',
                            'method' => 'post',
                            'submit_button' => $this->getButton("reg",'<i class="fa fa-check" aria-hidden="true"></i> Сохранить', 'btn'),
                            'cancel_button' => $this->getButton("reg",'<i class="fa fa-repeat" aria-hidden="true"></i> Отменить', 'btn cancel'),
                            'user_name' => '<div class="u-name"> <i class="fa fa-user" aria-hidden="true"></i> '.$this->getData('user_name').'</div>',
                            'exit_button' => $this->getButton("index.php?action=logout",'<i class="fa fa-power-off" aria-hidden="true"></i> Выход', "exit-button"),
                            
                            'cell_table' => $this->getData('cell_table'),
                            'add_info' =>  $this->getTemplate('register-form'),
                            'field_list'=>$this->getData('field_list')
                         ]);
        
       return $tpl -> processData();
    }
    
    /*
    function displayRegisterDefaultPage(){
        $tpl = new Templator;
        $tpl -> loadTemplate('add-patient');
        $js = $this->linkJsScript( J_QUERY );
        $js .= $this->linkJsScript( 'usr.js' );
        $js .= $this->linkJsScript( 'jquery.shuffleLetters.js' );
        $css = $this->linkStylesheets( 'reset.css' );
        $css .= $this->linkStylesheets( 'main.css' );
        $css .= $this->linkStylesheets( 'add-patient.css' );
        $css .= $this->linkStylesheets( 'header.css' );
        $css .= $this->linkStylesheets( 'footer.css' );

        $tpl -> loadData([  'header' => $this->getTemplate('header'),
                            'css' => $css, 'js' => $js,
                            'charset' => 'utf-8',
                            'lang' => 'ru',
                            'title' => 'АРМ Sonato {T:pwd}',
                            'footer' => $this->getTemplate('footer'),
                            'pwd' => '/ Новый посетитель',
                            'users' => $this-> getData('users'),
                            'id' => 'reg',
                            'name' => 'reg',
                            'action' => 'index.php?action=register',
                            'method' => 'post',
                            'submit_button' => $this->getButton("reg","Сохранить", 'btn'),
                            'cancel_button' => $this->getButton("reg","Отменить", 'btn cancel'),
                            'exit_button' => $this->getButton("index.php?action=logout","Выход", 'exit-button btn'),
                            'user_name' => $this->getData('user_name'),
                            'cell_table' => $this->getData('cell_table'),
                            'add_info' =>  $this->getTemplate('register-form'),
                            'field_list'=>$this->getData('field_list')
                         ]);
        
       return $tpl -> processData();
    }*/
} // class View ends