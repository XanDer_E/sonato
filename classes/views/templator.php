<?php
defined('ENGINE_ADM') or die('Доступ запрещен');

class Templator // шаблонизатор
{
	/* Шаблон для поиска сущностей */
	const _PATTERN = '/\{T\:[a-zA-Z_]{1}[a-zA-Z_0-9]*\}/';
	
	// Свойства
	private $template;
	private $data;
	private $pattern;
	private $temp;
	private $result;
	
	/* Конструктор. Инициализация свойств по-умолчанию */
	function __construct() {
		$this->template = '';
		$this->pattern = self::_PATTERN;
		$this->data = array();
		$this->temp = '';
		$this->result = '';
	}
	
	/* Загрузка шаблона в шаблонизатор */
	public function loadTemplate( $tpl_file_name ) {
		if( $this->template = file_get_contents(TPL_FOLDER.'/'.$tpl_file_name.'.tpl') ) {
			return true;			
		} else {
			return false;
		}
	}
	
	/* Загрузка данных в шаблонизатор */
	public function loadData( $data = array() ) {
		return $this->data = $data;
	}

	/* Обработка данных:
		Итеративное заполнение шаблона сущностями;
		Сохранение промежуточных результатов в свойстве temp во время каждой итерации; 
	*/
	public function processData() {
		$this->temp = $this->template;
		
		while( $this->fillTemplate() ) {
			continue;
		}
			$this->result = $this->temp;
			$this->temp = false;
		return $this->result;
	}		
	
	/* Заполнение шаблона сущностями */
	private function fillTemplate() {
		$matches = array();
		
		if( $this->temp == '' ) {
			return false;
		
		} else {
			$fields = array();
			preg_match_all( $this->pattern, $this->temp, $matches );
			
			if( empty($matches[0]) ) {
				return false;
			
			} else {	
				foreach($matches[0] as $k => $value) {
					if( array_key_exists( $k = strtok($matches[0][$k], '{T: }'), $this->data ) ) {
						$fields['{T:'.$k.'}'] = $this->data[$k];
					}
				}
				
				if ( empty($fields) ) {
					return false;
				} else {
					$this->temp = str_replace(array_keys($fields), $fields, $this->temp);
					return $this->temp;
				}
			}
		}
	}
	
}// End_Templator
